package com.example.hayden.tictactoe;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.Activity;
import android.widget.TextView;
import android.widget.Button;




public class TicTacToe extends ActionBarActivity {

    private TicTacToeData board_;
    TextView textView;
    Button b[][];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);


        setBoard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tic_tac_toe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        setBoard();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setBoard() {

        textView = (TextView) findViewById(R.id.dialogue);

        board_ = new TicTacToeData(3);

        int one, two, three, four, five, six, seven, eight, nine;
        one = board_.getBoardIndex(0, 0);
        two = board_.getBoardIndex(0, 1);
        three = board_.getBoardIndex(0, 2);
        four = board_.getBoardIndex(1, 0);
        five = board_.getBoardIndex(1, 1);
        six = board_.getBoardIndex(1, 2);
        seven = board_.getBoardIndex(2, 0);
        eight = board_.getBoardIndex(2, 1);
        nine = board_.getBoardIndex(2, 2);

        b = new Button[3][3];

        b[0][0] = (Button)findViewById(R.id.one);
        b[0][1] = (Button)findViewById(R.id.two);
        b[0][2] = (Button)findViewById(R.id.three);

        b[1][0] = (Button)findViewById(R.id.four);
        b[1][1] = (Button)findViewById(R.id.five);
        b[1][2] = (Button)findViewById(R.id.six);

        b[2][0] = (Button)findViewById(R.id.seven);
        b[2][1] = (Button)findViewById(R.id.eight);
        b[2][2] = (Button)findViewById(R.id.nine);

        textView.setText("Press a button to start");

        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                b[i][j].setOnClickListener(new MyClickListener(i, j));
                if (!b[i][j].isEnabled()) {
                    b[i][j].setText(" ");
                    b[i][j].setEnabled(true);
                }
            }
        }
    }

    class MyClickListener implements View.OnClickListener {
        int x;
        int y;

        public MyClickListener(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public void onClick(View view) {
            if (b[x][y].isEnabled()) {
                b[x][y].setEnabled(false);

                if (board_.getTurn() == 0)
                    b[x][y].setText("O");
                else
                    b[x][y].setText("X");

                board_.processTurn(x, y);
                textView.setText("");
            }
            checkBoard();
        }
    }

    public boolean checkBoard() {
        boolean gameOver = false;
        int one, two, three, four, five, six, seven, eight, nine;
        one = board_.getBoardIndex(0, 0);
        two = board_.getBoardIndex(0, 1);
        three = board_.getBoardIndex(0, 2);
        four = board_.getBoardIndex(1, 0);
        five = board_.getBoardIndex(1, 1);
        six = board_.getBoardIndex(1, 2);
        seven = board_.getBoardIndex(2, 0);
        eight = board_.getBoardIndex(2, 1);
        nine = board_.getBoardIndex(2, 2);

        if ((one == 0 && five == 0 && nine == 0) // diagonal top-left to bottom-right
                || (three == 0 && five == 0 && seven == 0) // diagonal top-right to bottom-left
                || (three == 0 && six == 0 && nine == 0) // vertical right
                || (two == 0 && five == 0 && eight == 0) // vertical middle
                || (one == 0 && four == 0 && seven == 0) // vertical left
                || (one == 0 && two == 0 && three == 0) // horizontal top
                || (four == 0 && five == 0 && six == 0) // horizontal middle
                || (seven == 0 && eight == 0 && nine == 0)) { // horizontal bottom
            textView.setText("Game over. O wins!");
            gameOver = true;
        } else if ((one == 1 && five == 1 && nine == 1) // diagonal top-left to bottom-right
                || (three == 1 && five == 1 && seven == 1) // diagonal top-right to bottom-left
                || (three == 1 && six == 1 && nine == 1) // vertical right
                || (two == 1 && five == 1 && eight == 1) // vertical middle
                || (one == 1 && four == 1 && seven == 1) // vertical left
                || (one == 1 && two == 1 && three == 1) // horizontal top
                || (four == 1 && five == 1 && six == 1) // horizontal middle
                || (seven == 1 && eight == 1 && nine == 1)) { // horizontal bottom
            textView.setText("Game over. X wins!");
            gameOver = true;
        } else {
            boolean empty = false;
            for(int i=0; i<=2; i++) {
                for(int j=0; j<=2; j++) {
                    if( board_.getBoardIndex(i, j) == 2) {
                        empty = true;
                        break;
                    }
                }
            }
            if(!empty) {
                gameOver = true;
                textView.setText("Game over. It's a draw!");
            }
        }
        return gameOver;
    }
}