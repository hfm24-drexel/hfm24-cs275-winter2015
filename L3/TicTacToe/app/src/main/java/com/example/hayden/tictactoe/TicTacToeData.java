package com.example.hayden.tictactoe;

/**
 * Created by Hayden on 2/17/2015.
 */
public class TicTacToeData {

    private int [][] board_;
    private int turn_;

    public TicTacToeData (int size) {
        board_ = new int[size][size];
        turn_ = 0; // O goes first

        for (int i = 0; i < board_.length; i++) {
            for (int j = 0; j < board_[i].length; j++) {
                board_[i][j] = 2;
            }
        }
    }

    public void processTurn (int row, int col) {
        if (turn_ == 0) {
            board_[row][col] = 0;
            turn_ = 1;
        }
        else {
            board_[row][col] = 1;
            turn_ = 0;
        }
    }

    public int getTurn () {
        return turn_;
    }

    public int [][] getBoard() {
        return board_;
    }

    public int getBoardIndex(int row, int col) {
        return board_[row][col];
    }


}
