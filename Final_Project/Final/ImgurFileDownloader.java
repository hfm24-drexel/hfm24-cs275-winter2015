import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import photographicmosaic.engine.AnalyzeImage;

public class ImgurFileDownloader {
	/* NOTE: In order for this program to work, you NEED these in the same file location as this class:
		- a folder called "Image"
		- a text file called "oauthTokensFile.txt"
		- a text file called "pageAndIndexFile.txt"
	*/
	
	static String clientID = "f811fcb7f72f4c6";
	static String clientSecret = "667b112dc8cb0c64a13003a8cf187725d5c89a5e";
	static String access_token, refresh_token;
	static String oauthTokenFile = "oauthTokensFile.txt";
	static String pageAndIndexFile = "pageAndIndexFile.txt";


	static String convertStreamToString(InputStream is) {
 		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
	
	public static void main( String [] args ) throws Exception{

		String refreshToken = getRefreshTokenFromFile(oauthTokenFile); // Getting stored refresh token from previous sessions
		if (refreshToken == null) {	// If it doesn't exist...
			Authorization(); // Conduct OAuth2 Authorization
		}
		else {	// We're good, get a new access token
			refresh_token = refreshToken;
			AuthorizationWithRefresh();
		}

		int searchSize = 40; // Size of the database to be stored
		int[] pageAndIndex = getPageAndIndex(pageAndIndexFile); 
		int page = pageAndIndex[0];	// Page of the subreddit to search
		int index = pageAndIndex[1];	// Index of the entire database, which is different from the i, the index of the current search.
		String subreddit = "wallpapers";	// Name of the subreddit

		String[] imageURLs = new String[searchSize]; 	// Array of image URLs to be converted into bitmap images
		int i = 0;// Index of the imageURLs to be filled
	
		ExecutorService pool = Executors.newFixedThreadPool(searchSize);
	
		while (imageURLs[searchSize-1] == null) { 	// While the array of URLs isn't full...

			System.out.println("Search page " + Integer.toString(page) + " of subreddit " + subreddit);
			String sGallery_url = "https://api.imgur.com/3/gallery/r/" + subreddit + "/" + Integer.toString(page);
			String authorizationParameters = "bearer=" + access_token;
			URL gallery_url = new URL(sGallery_url);
			HttpURLConnection connection = (HttpURLConnection) gallery_url.openConnection();
			// Filling header with access token
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Bearer " + access_token);
			connection.connect();	
	
			InputStream galleryListings = (InputStream) (connection.getContent());
			String galleryString = convertStreamToString(galleryListings);
//			System.out.println(galleryString);
//			String getGalleryResponse = executeGET(sGallery_url, authorizationParameters);

			// Get result as a Json Array and filter the results. Then put them in a folder.
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(galleryString);
			JsonObject rootobj = root.getAsJsonObject();
			JsonArray data = rootobj.get("data").getAsJsonArray();

				
			for (JsonElement image : data) {
				if (imageURLs[searchSize-1] == null) {
					String imageURL = image.getAsJsonObject().get("link").getAsString();
					boolean nsfw = image.getAsJsonObject().get("nsfw").getAsBoolean();
					int width = image.getAsJsonObject().get("width").getAsInt();
					int height = image.getAsJsonObject().get("height").getAsInt();

					if (nsfw == false && width == 1920 && height == 1080) {	// Filtering based on size and nsfw-ness
						imageURLs[i] = imageURL;
						System.out.println("Found Image! Filling index " + Integer.toString(i) + " of " + Integer.toString(searchSize-1) + " (" + index + " images overall)");
						final String _imageURL = imageURLs[i];
						final int _index = index;
						pool.execute(new Runnable() {
							public void run() {
								try {
									saveImage(_imageURL, "Images/image" + _index + ".jpg");
									AnalyzeImage.analyzeImage("Images/image" + _index + ".jpg", _imageURL);	// Amndeep's code
								} catch (IOException e) {
									e.printStackTrace();
								} catch (ClassNotFoundException e) {
									e.printStackTrace();
								} catch (InstantiationException e) {
									e.printStackTrace();
								} catch (SQLException e) {
									e.printStackTrace();
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								}




							}
						});
						index++;
						i++;
					}
				}
			}

			page++;
		}
		pool.shutdown();
		while (!pool.isTerminated()) {
			try {
				Thread.sleep(100);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}

		// Storing the current page and index the search stopped at, for future searches using a cronjob
		String[] currentPageAndIndex = new String[2]; 
		currentPageAndIndex[0] = Integer.toString(page);
		currentPageAndIndex[1] = Integer.toString(index);
		writeToFile(currentPageAndIndex, pageAndIndexFile);		
	}

	public static void saveImage(String imageURL, String destinationFile) throws IOException {
		URL url = new URL(imageURL);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);

		byte[] b = new byte[2048];
		int length;
		while ((length = is.read(b)) != -1)
			os.write(b, 0, length);

		is.close();
		os.close();
	}


	public static void Authorization() { // OAuth2 authorization
		Scanner in = new Scanner(System.in);
		String OAuthURL = "https://api.imgur.com/oauth2/authorize?client_id=" + clientID + "&response_type=token";
		System.out.println("Go to the following URL: " + OAuthURL);
		System.out.println("Type in the access token found in the URL after granting permission: ");
		
		access_token = in.nextLine(); 
		
		System.out.println("\nType in the refresh token found in the URL: ");

		refresh_token = in.nextLine();

		writeToFile(refresh_token, oauthTokenFile);
	}

	public static void AuthorizationWithRefresh() { // Executing oauth authorization using a refresh token
		String params = "refresh_token=" + refresh_token + "&client_id=" + clientID + "&client_secret=" + clientSecret + "&grant_type=refresh_token";
		String response = executePost("https://api.imgur.com/oauth2/token", params);

		JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(response);
                JsonObject rootobj = root.getAsJsonObject();
		access_token = rootobj.get("access_token").getAsString();
		refresh_token = rootobj.get("refresh_token").getAsString();

		writeToFile(refresh_token, oauthTokenFile); // Saving refresh token for future use
	
		
	}

	public static String executePost( String targetURL, String params) {

                URL url;
                HttpURLConnection connection = null;

                try {
                        url = new URL(targetURL);

                        connection = (HttpURLConnection)url.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                        connection.setRequestProperty("Content-Length", "" + Integer.toString(params.getBytes().length));
                        connection.setRequestProperty("Content-Language", "en-US");
                        connection.setUseCaches(false);
                        connection.setDoInput(true);
                        connection.setDoOutput(true);

                        // Send request
                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
                        wr.writeBytes(params);
                        wr.flush();
                        wr.close();

                        // Get response
                        InputStream is = connection.getInputStream();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                        String line;
                        StringBuffer response = new StringBuffer();
                        while ((line = rd.readLine()) != null) {
                                response.append(line);
                                response.append('\r');
                        }
                        rd.close();
                        return response.toString();
                }
                catch (Exception e) {
                        e.printStackTrace();
                        return null;
                }
                finally {

                        if (connection != null) {
                                connection.disconnect();
                        }
                }
        }


	public static String getRefreshTokenFromFile(String filePath) {

		String response = null;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));

			String refreshToken = br.readLine();
			if (refreshToken != null) {
				response = refreshToken;
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;

	}

	public static int[] getPageAndIndex(String filePath) {
		int[] response = new int[] {0, 0};
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));

			String page = br.readLine();
			if (page != null) {
				response[0]  = Integer.parseInt(page);
			}
		
			String index = br.readLine();
			if (index != null) {
				response[1] = Integer.parseInt(index);
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;


	}

	public static void writeToFile(String line, String filePath) {
			try {
				PrintWriter writer = new PrintWriter(filePath, "UTF-8");
				writer.println(line);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	public static void writeToFile(String[] lines, String filePath) {
			try {
				PrintWriter writer = new PrintWriter(filePath, "UTF-8");
				for (int i = 0; i < lines.length; i++) {
					writer.println(lines[i]);
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
}
