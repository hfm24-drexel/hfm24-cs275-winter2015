package photographicmosaic.imageio;

import java.awt.image.BufferedImage;

public class ImageAnalysis{

	public static double[] averagePixelValues(BufferedImage image){
		return averagePixelValuesPerArea(image, 0, 0, image.getWidth(), image.getHeight());
	}

	public static double[] averagePixelValuesPerArea(BufferedImage image, int xstart, int ystart, int width, int height){
		double[] ret = new double[3];
		for(int y = ystart; y < ystart + height; y++){
			for(int x = xstart; x < xstart + width; x++){
				double[] temp_rgb = new double[3];
				temp_rgb[0] = (image.getRGB(x, y) >> 16) & 0x000000FF;
				temp_rgb[1] = (image.getRGB(x, y) >> 8) & 0x000000FF;
				temp_rgb[2] = image.getRGB(x, y) & 0x000000FF;

				double[] temp_cie = xyzToCIE_L_times_ab(rgbToXYZ(temp_rgb));

				ret[0] += temp_cie[0];
				ret[1] += temp_cie[1];
				ret[2] += temp_cie[2];
			}
		}

		ret[0] /= width * height;
		ret[1] /= width * height;
		ret[2] /= width * height;

		return ret;
	}

	/**
	 * Converts the color values of an image from RGB form to XYZ form. Credit for the algorithm goes to http://www.easyrgb.com/index.php?X=MATH and to Wikipedia for the explanations.
	 */
	public static double[] rgbToXYZ(double[] colors){
		double[] rgb = {colors[0] / 255.0, colors[1] / 255.0, colors[2] / 255.0};

		for(double c : rgb){
			if(c > 0.04045){
				c = Math.pow((c + 0.055) / 1.055, 2.4);
			}
			else{
				c /= 12.92;
			}

			c *= 100;
		}

		double[] xyz = new double[3];
		xyz[0] = rgb[0] * 0.4124 + rgb[1] * 0.3576 + rgb[2] * 0.1805;
		xyz[1] = rgb[0] * 0.2126 + rgb[1] * 0.7152 + rgb[2] * 0.0722;
		xyz[2] = rgb[0] * 0.0193 + rgb[1] * 0.1192 + rgb[2] * 0.9505;

		return xyz;
	}

	/**
	 * Converts the color values of an image from XYZ form to CIELab form. Credit for the algorithm goes to http://www.easyrgb.com/index.php?X=MATH and to Wikipedia for the explanations.

	 */
	public static double[] xyzToCIE_L_times_ab(double[] colors){
		double refX = 95.047;
		double refY = 100.000;
		double refZ = 108.883;

		double xyz[] = new double[3];
		xyz[0] = colors[0] / refX;
		xyz[1] = colors[1] / refY;
		xyz[2] = colors[2] / refZ;

		for(double c : xyz){
			if(c > 0.008856){
				c = Math.pow(c, (1.0 / 3.0));
			}
			else{
				c = (7.787 * c) + (16.0 / 116.0);
			}
		}

		double lab[] = new double[3];
		lab[0] = (116 * xyz[0]) - 16;
		lab[1] = 500 * (xyz[0] - xyz[1]);
		lab[2] = 200 * (xyz[1] - xyz[2]);

		return lab;
	}
}
