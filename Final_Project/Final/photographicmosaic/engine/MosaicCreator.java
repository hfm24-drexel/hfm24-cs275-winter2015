package photographicmosaic.engine;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.imageio.ImageIO;

import photographicmosaic.imageio.ImageAnalysis;
import photographicmosaic.imageio.ImageManipulator;

public class MosaicCreator{
	public static int bestImage(double[] source) throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException{
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection c = DriverManager.getConnection("jdbc:mysql://db4free.net", "application", "123456abcd");

		Statement stmt = c.createStatement();

		String query = "SELECT ID FROM drexelcs275.data ORDER BY POW((drexelcs275.data.first - " + source[0] + "), 2) + POW((drexelcs275.data.second - " + source[1] + "), 2) + POW((drexelcs275.data.third = " + source[2] + "), 2) LIMIT 1";

		ResultSet rs = stmt.executeQuery(query);
		rs.next();
		return rs.getInt("ID");
	}

	public static byte[] createMosaic(byte[] sourceBytes) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException, IOException, SQLException{
		InputStream in = new ByteArrayInputStream(sourceBytes);
		final BufferedImage source = ImageIO.read(in);

		final int rows = 10;
		final int columns = 10;

		final int width = source.getWidth();
		final int height = source.getHeight();

		final int[][] selectedImages = new int[rows][columns];

		ExecutorService pool = Executors.newCachedThreadPool();

		for(int y = 0; y < rows; y++){
			final int rowheight = (int)(Math.floor(height / ((double) rows)));

			final int yy = y;
			pool.execute(new Runnable(){
				public void run(){
					for(int x = 0; x < columns; x++){
						final int columnwidth = (int)(Math.floor(width / ((double) columns)));

						double[] ave = ImageAnalysis.averagePixelValuesPerArea(source, x * columnwidth, yy * rowheight, columnwidth, rowheight);
						try{
							selectedImages[yy][x] = bestImage(ave);
						}
						catch(ClassNotFoundException e){

						}
						catch(IllegalAccessException e){

						}
						catch(InstantiationException e){

						}
						catch(SQLException e){

						}
					}
				}
			});
		}

		pool.shutdown();

		while(!pool.isTerminated()){
			Thread.sleep(500);
		}

		BufferedImage[][] selectedBufferedImages = new BufferedImage[rows][columns];
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection c = DriverManager.getConnection("jdbc:mysql://db4free.net", "application", "123456abcd");
		Statement stmt = c.createStatement();
		for(int y = 0; y < rows; y++){
			for(int x = 0; x < columns; x++){
				String query = "SELECT Image FROM drexelcs275.data WHERE ID=" + selectedImages[y][x];
				ResultSet rs = stmt.executeQuery(query);
				rs.next();
				byte[] bytes = rs.getBytes("Image");
				in = new ByteArrayInputStream(bytes);
				selectedBufferedImages[y][x] = ImageIO.read(in);
			}
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(ImageManipulator.createImage(selectedBufferedImages), "jpg", baos);
		baos.flush();
		return baos.toByteArray();
	}
}
