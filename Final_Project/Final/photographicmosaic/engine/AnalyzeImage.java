package photographicmosaic.engine;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.*;
import javax.imageio.ImageIO;

import photographicmosaic.imageio.ImageAnalysis;
import photographicmosaic.imageio.ImageManipulator;

public class AnalyzeImage{
	public static void analyzeImage(String filename, String url) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, SQLException{
		BufferedImage image = ImageManipulator.getImage(filename);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		baos.flush();
		byte[] bytes = baos.toByteArray();
		baos.close();
		double[] averageColorValues = ImageAnalysis.averagePixelValues(image);

		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection c = DriverManager.getConnection("jdbc:mysql://db4free.net", "application", "123456abcd");

		PreparedStatement pre = c.prepareStatement("INSERT INTO drexelcs275.data (Image, URL, first, second, third) VALUES (?, ?, ?, ?, ?)");

		pre.setBytes(1, bytes);
		pre.setString(2, url);
		pre.setDouble(3, averageColorValues[0]);
		pre.setDouble(4, averageColorValues[1]);
		pre.setDouble(5, averageColorValues[2]);
		pre.executeUpdate();

		pre.close();
		c.close();
	}
}
