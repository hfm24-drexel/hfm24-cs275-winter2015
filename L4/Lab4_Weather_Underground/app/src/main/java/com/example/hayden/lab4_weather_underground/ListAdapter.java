package com.example.hayden.lab4_weather_underground;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View.OnClickListener;

import java.util.List;

public class ListAdapter extends ArrayAdapter< Forecast > {
    /*
        Custom array adapter, which takes a list of forecasts as its data input.
        The adapter fills in a listView, which has as its items a list of the forecast's
        date, condition image (cloudy, sunny, rain, etc.), condition, temperature, and humidity.
     */


    private final Context context;
    private final List<Forecast> forecasts;

    public ListAdapter(Context context, List<Forecast> forecasts) {
        super(context, R.layout.activity_wunderground, forecasts);
        this.context = context;
        this.forecasts = forecasts;
    }

    @Override
    public View getView(final int position,
                        View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // view which fills in the necessary forecast information in the list_item_layout
        View rowView = inflater.inflate(R.layout.list_item_layout,
                parent, false);

        // Getting all the views in rowView
        TextView date = (TextView) rowView.findViewById(R.id.textView);
        TextView condition = (TextView) rowView.findViewById(R.id.textView2);
        TextView temperature = (TextView) rowView.findViewById(R.id.textView3);
        TextView humidity = (TextView) rowView.findViewById(R.id.textView4);
        ImageView conditionImage = (ImageView) rowView.findViewById(R.id.imageView);

        // Setting the text views in rowView
        date.setText("Date: " + forecasts.get(position).getDate());
        condition.setText("Condition: " + forecasts.get(position).getCondition());
        temperature.setText("Temperature: " + forecasts.get(position).getTemperature() + " F");
        humidity.setText("Humidity: " + forecasts.get(position).getHumidity());

        // Setting the image view in rowView. Because the image is retrieved from a url, the image must be set with an async task.
        Condition_Image condition_image = new Condition_Image(forecasts.get(position).getConditionURL(), conditionImage);
        condition_image.execute();

        return rowView;
    }

}
