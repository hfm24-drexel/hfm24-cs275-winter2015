package com.example.hayden.lab4_weather_underground;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Hayden on 3/4/2015.
 */
public class Condition_Image extends AsyncTask<Void, Void, Void> {
    /*
            Updates the input imageView to show the image found in the input url
     */
    String url;
    Bitmap imgBitmap;
    ImageView imageView;

    public Condition_Image(String _url, ImageView _imageView) {
        url = _url;
        imageView = _imageView;
    }

    @Override
    protected Void doInBackground(Void... params) {
        // Networking...

        try {
            // Connecting to the url
            String sURL = url;
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            // Converting the resulting input stream into a bitmap
            InputStream result = request.getInputStream();
            imgBitmap = BitmapFactory.decodeStream(result);




        } catch(Exception e) {
            Log.e("ERROR", e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void params) {
        // Updating the imageView
        imageView.setImageBitmap(imgBitmap);
    }
}
