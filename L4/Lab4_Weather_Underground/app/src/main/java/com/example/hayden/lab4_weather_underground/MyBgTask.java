package com.example.hayden.lab4_weather_underground;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;


import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Hayden on 2/24/2015.
 */
public class MyBgTask extends AsyncTask<Void, Void, Void> {
    /*
        Finds the location of the input key using Wunderground's geolookup function,
        Then passes that, the key, the adapter, and the forecast list onto another async task,
        which then fills in the forecast list.
     */

    List< Map<String, String> > data;
    ListAdapter adapter;
    String key, zip;
    List<Forecast> forecasts;

    public MyBgTask(ListAdapter _adapter,
                    String _key,
                    List<Forecast> _forecasts) {
        key = _key;
        adapter = _adapter;
        forecasts = _forecasts;
    }

    @Override
    protected Void doInBackground(Void... params) {
        // Networking...


        try {
            // Finding the zip code
            String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            // Convert to a JSON object to print data
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

            // Get zip code from the input key
            zip = rootobj.get("location").getAsJsonObject().get("zip").getAsString();


        } catch(Exception e) {
            Log.e("ERROR", e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void params) {
        // Calling the async task which will fill in the forecast list
        Hourly_Forecast hourly_forecast = new Hourly_Forecast(adapter, zip, key, forecasts);

        hourly_forecast.execute();

    }
}
