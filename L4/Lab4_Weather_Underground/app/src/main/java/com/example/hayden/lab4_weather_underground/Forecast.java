package com.example.hayden.lab4_weather_underground;

/**
 * Created by Hayden on 3/4/2015.
 */
public class Forecast {
    /*
        Class that holds data of the forecast for a single hour of the day.
     */

    private String date, conditionURL, condition, temperature, humidity;

    public Forecast( String _date,
                     String _conditionURL,
                     String _condition,
                     String _temperature,
                     String _humidity) {

        date = _date;
        conditionURL = _conditionURL;
        condition = _condition;
        temperature = _temperature;
        humidity = _humidity;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getConditionURL() {
        return conditionURL;
    }

    public void setConditionURL(String conditionURL) {
        this.conditionURL = conditionURL;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }


}
