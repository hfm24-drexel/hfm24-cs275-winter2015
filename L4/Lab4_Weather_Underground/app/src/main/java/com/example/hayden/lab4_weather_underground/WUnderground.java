package com.example.hayden.lab4_weather_underground;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class WUnderground extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wunderground);

        // List of forecasts to be filled by an Async task.
        List<Forecast> forecasts = new ArrayList<Forecast>();

        // Custom adapter that will forecast information in a list view
        ListAdapter adapter = new ListAdapter(this, forecasts);

        // My own wunderground key for finding location to output the weather forecast for
        String key = "1194b8c48389ec51";
        // I know, I know...

        ListView l = (ListView) findViewById(R.id.listView);
        l.setAdapter(adapter);

        MyBgTask bgTask = new MyBgTask(adapter, key, forecasts);

        bgTask.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wunderground, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
