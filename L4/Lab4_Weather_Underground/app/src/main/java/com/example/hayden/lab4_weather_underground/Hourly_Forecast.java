package com.example.hayden.lab4_weather_underground;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;


import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hayden on 3/4/2015.
 */
public class Hourly_Forecast extends AsyncTask<Void, Void, Void> {
    /*
        Fills in the forecast list using Wunderground's hourly forecast function.
        Updates the adapter after filling in the list.
     */

    ListAdapter adapter;
    String zip, key;
    List<Forecast> forecasts;

    public Hourly_Forecast(ListAdapter _adapter,
                           String _zip,
                           String _key,
                           List<Forecast> _forecasts) {
        adapter = _adapter;
        zip = _zip;
        key = _key;
        forecasts = _forecasts;
    }

    @Override
    protected Void doInBackground(Void... params) {
        // Networking...

        try {
            // Getting the hourly forecast
            String sURL = "http://api.wunderground.com/api/" + key + "/hourly/q/" + zip + ".json";
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            // Converting the result to a Json Object and getting the hourly forecast array
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootobj = root.getAsJsonObject();
            JsonArray hourly_forecast = rootobj.get("hourly_forecast").getAsJsonArray();

            // Loop through the hourly forecast array, getting the date, condition image URL, condition, temperature, and humidity
            for (JsonElement forecast : hourly_forecast)
            {
                JsonObject fc = forecast.getAsJsonObject();
                String date = fc.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
                String condition = fc.getAsJsonObject().get("condition").getAsString();
                String conditionImageURL = fc.getAsJsonObject().get("icon_url").getAsString();
                String temp_f = fc.getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
                String humidity = fc.getAsJsonObject().get("humidity").getAsString();

                Forecast newForecast = new Forecast(date, conditionImageURL, condition, temp_f, humidity);
                forecasts.add(newForecast);
            }


        } catch(Exception e) {
            Log.e("ERROR", e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void params) {

        // Updating the adapter
        adapter.notifyDataSetChanged();
    }
}
