Lab 4: Weather Underground Hourly Forecast Retriever

This program retrieves the hourly forecast for the next 24 hours in Philadelphia from Weather Underground's web api, and displays the date /time, conditions, temperature, and humidity, along with an image showing the conditions (cloudy, sunny, etc.) for each hour. No user input is needed.

Other students in group:
cfz23