Author: Hayden McPhee
Class: CS275-004
Purpose:

	The TwitterWordCount program uses Temboo to get a specified number of tweets from a twitter user, counts the number of syllables in each word using wordnik's hyphenate feature, and returns a grade based on the overall syllable count. To use the program, there are several steps that need to be taken.

1. Enter the client ID of your twitter application.
2. Enter the client secret of your twitter application.
3. Enter your Temboo account name.
4. Enter your Temboo app name.
5. Enter your Temboo app key.

After these steps have been completed, the program will tell you to go to a url to give the program permission to use all of the information you just provided. Once you have given that permission, return to the console and type anything, then hit enter. This is just to tell the program that you have actually given it permission. Next are these steps:

1. Enter the user name of the user you want to get tweets from. There should be no spaces and no @ symbol.
2. Enter the number of tweets you want to get. Obviously, this must be a positive number.
3. Enter your wordnik API key. If you don't have one, simply type 'n' to use the default.

That is all of the user input needed for the program. After that, the program will output a list of polysyllabic words (more than 3 syllables) and their number of syllables. This may take a while, as going through each word takes about half a second. Once all polysyllabic words have been found, the program puts the total number of them through a formula and returns the estimated school grade level necessary to comprehend that user's tweets. The formula looks like this:

		grade = 1.0430 * sqrt(number_of_polysyllables * (30 / number_of_tweets)) + 3.1291 

Included IDEs: com/temboo, com/google, org/


