import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

// Json Imports
import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

// Temboo Twitter Imports
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.Library.Twitter.Users.Search;
import com.temboo.Library.Twitter.Users.Search.SearchInputSet;
import com.temboo.Library.Twitter.Users.Search.SearchResultSet;

// Temboo Wordnik Imports
import com.temboo.Library.Wordnik.Word.GetHyphenation;
import com.temboo.Library.Wordnik.Word.GetHyphenation.GetHyphenationInputSet;
import com.temboo.Library.Wordnik.Word.GetHyphenation.GetHyphenationResultSet;


// General Temboo Imports
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class TwitterWordCount {

	public static void main ( String [] args ) throws Exception {

		String client_id, client_secret, temboo_acct_name, temboo_app_name, temboo_app_key;

		Scanner input = new Scanner(System.in);

		client_id = "87EdZEVLhDw7Plajyg71XxVGY";
		System.out.println("Enter client ID: ");
		client_id = input.nextLine();

		client_secret = "1XAN0xXKrcypX9BqOD9UKoVMnDZgS8sYRGXYZrXli9g35MGlMS";
		System.out.println("Enter client secret: ");
		client_secret = input.nextLine();

		temboo_acct_name = "hfm24";
		System.out.println("Enter Temboo account name: ");
		temboo_acct_name = input.nextLine();

		temboo_app_name = "myFirstApp";
		System.out.println("Enter Temboo app name: ");
		temboo_app_name = input.nextLine();

		temboo_app_key = "28540b9904434749b063fb3cedc4ec45";
		System.out.println("Enter Temboo app key: ");
		temboo_app_key = input.nextLine();

		

		// Authorization and getting the results:
		TembooSession session = new TembooSession(temboo_acct_name, temboo_app_name, temboo_app_key);	
		String [] OAuthResults = executeTembooAuthorization(client_id, client_secret, session);
		
		String accessToken, accessTokenSecret;

		accessToken = OAuthResults[0];
		accessTokenSecret = OAuthResults[1];

		// Done authorizing.


		String searchForUser = "BarackObama";
		System.out.println("Enter the user name of the user you want to get tweets from: ");
		searchForUser = input.nextLine();

		// Searching for desired user:
		String [] searchResults = findUser(accessToken, accessTokenSecret, client_id, client_secret, searchForUser, session);

		String userID = searchResults[0];
		String screenName = searchResults[1];		

		// Using results to find desired number of tweets:
		int nTweets = 15;
		System.out.println("Enter the amount of tweets you want to get: ");
		nTweets = Integer.parseInt(input.nextLine());

		String [] tweetResults =  getTwitterWords (accessToken,
                                                	accessTokenSecret,
                                                	client_id,
                                                	client_secret,
                                                	screenName,
                                                	userID,
                                                	nTweets,
                                                	session);

		String wordnik_api_key = "f7bc6e0370639af3fdb0c073af90452ea5ed2d64e0bd6d712";
		System.out.println("Enter your wordnik api key, or type 'n' to use the default: ");
		String in = input.nextLine();
		if (!in.equals("n"))
			wordnik_api_key = in;

		String [][] syllabizedResults = new String[nTweets][tweetResults.length];
		int nPolysyllabicWords = 0;
		for (int i = 0; i < tweetResults.length; i++) {
			syllabizedResults[i] = syllabizeSentence(wordnik_api_key, tweetResults[i], session);
			for (int j = 0; j < syllabizedResults[i].length; j++) {
				if (syllabizedResults[i][j] != null) {
					nPolysyllabicWords += 1;
				}
			}
		}

		double grade = 1.0430 * Math.sqrt(nPolysyllabicWords * 30 / nTweets) + 3.1291;

		System.out.println("Grade: " + grade);
	}


	public static String [] executeTembooAuthorization (String client_id, 
						String client_secret, 
						TembooSession session) throws TembooException {

		/* Executes OAuth for Twitter using Temboo. Returns an array of strings. In the array are:
			0. Access Token
			1. Access Token Secret
			2. Screen Name
			3. User ID
		With the numbers corresponding to the array indices.
		*/


		Scanner in = new Scanner(System.in);
		String authorizationURL, callbackID, OAuthTokenSecret, accessToken, accessTokenSecret, screenName, userID;	

		// OAuth step 1:

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
	
		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
	
		// Set inputs
		initializeOAuthInputs.set_ConsumerSecret(client_secret);
		initializeOAuthInputs.set_ConsumerKey(client_id);
	
		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);

		authorizationURL = initializeOAuthResults.get_AuthorizationURL();
		callbackID = initializeOAuthResults.get_CallbackID();
		OAuthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret();

		System.out.println("Go here to give the app permission: " + authorizationURL);
		System.out.println("Type anything an hit enter to confirm that you have given permission");
		String input = in.nextLine();

		// Done with first step of OAuth



		// OAuth Step 2:

		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callbackID);
		finalizeOAuthInputs.set_OAuthTokenSecret(OAuthTokenSecret);
		finalizeOAuthInputs.set_ConsumerSecret(client_secret);
		finalizeOAuthInputs.set_ConsumerKey(client_id);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		accessToken = finalizeOAuthResults.get_AccessToken();
		accessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		screenName = finalizeOAuthResults.get_ScreenName();
		userID = finalizeOAuthResults.get_UserID();

		String [] OAuthResults = new String[4];
		OAuthResults[0] = accessToken;
		OAuthResults[1] = accessTokenSecret;
		OAuthResults[2] = screenName;
		OAuthResults[3] = userID;

		return OAuthResults;
		
		// Done with step 2 of OAuth
	}

	public static String [] findUser (String accessToken, 
					String accessTokenSecret,
					String client_id,
					String client_secret,
					String searchString,
					TembooSession session) throws TembooException{
		
		Search searchChoreo = new Search(session);

		// Get an InputSet object for the choreo
		SearchInputSet searchInputs = searchChoreo.newInputSet();

		// Set inputs
		searchInputs.set_AccessToken(accessToken);
		searchInputs.set_AccessTokenSecret(accessTokenSecret);
		searchInputs.set_ConsumerSecret(client_secret);
		searchInputs.set_ConsumerKey(client_id);
		searchInputs.set_SearchString(searchString);

		// Execute Choreo
		SearchResultSet searchResults = searchChoreo.execute(searchInputs);

		String unprocessedJson = searchResults.get_Response();

		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(unprocessedJson);
		JsonArray rootArray = root.getAsJsonArray();

		String userID = rootArray.get(0).getAsJsonObject().get("id").getAsString();
		String screenName = rootArray.get(0).getAsJsonObject().get("screen_name").getAsString();

		String [] results = new String[2];
		results[0] = userID;
		results[1] = screenName;
	
		return results;
	
	}


	public static String [] getTwitterWords (String accessToken,
						String accessTokenSecret,
						String client_id,
						String client_secret,
						String screenName,
						String userID,
						int nTweets,
						TembooSession session) throws TembooException {

		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_ScreenName(screenName);
		userTimelineInputs.set_AccessToken(accessToken);
		userTimelineInputs.set_AccessTokenSecret(accessTokenSecret);
		userTimelineInputs.set_ConsumerSecret(client_secret);
		userTimelineInputs.set_UserId(userID);
		userTimelineInputs.set_ConsumerKey(client_id);
		userTimelineInputs.set_Count(nTweets);
		
		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);

		String unprocessedJson = userTimelineResults.get_Response();

		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(unprocessedJson);
		JsonArray rootArray = root.getAsJsonArray();
		
		String [] tweets = new String[nTweets];

		int i = 0;
		for ( JsonElement tweet : rootArray) {
		
			String text = tweet.getAsJsonObject().get("text").getAsString();
			tweets[i] = text;
			i++;
		}
		
		return tweets;
	}

	
	public static String [] syllabizeSentence( String wordnik_api_key, String sentence, TembooSession session ) throws TembooException {
		
		// Remove all extra whitespaces and non-whitespace/alphabet/numerical characters
		String editedSentence = sentence.replaceAll("[^0-9a-zA-Z ]", "");
		editedSentence = editedSentence.replaceAll("\\s+", " "); 

		// Split the sentenct into individual words
		String [] parsedSentence = editedSentence.split(" ");

		// Two parallel arrays for storing the polysyllabic words and their numbers of syllables
		String [] polysyllabicWords = new String[parsedSentence.length];
		String [] nSyllables = new String[parsedSentence.length];
	
		for (int i = 0; i < parsedSentence.length; i++) { // For all words, evaluate the hyphens for each word
			if (parsedSentence[i].equals("")) { 
				// Replacing any empty indices with 'n', which returns 0 syllables, not affecting the outcome
				parsedSentence[i] = "n";
			}

			GetHyphenation getHyphenationChoreo = new GetHyphenation(session);

			// Get an InputSet Object for the choreo
			GetHyphenationInputSet getHyphenationInputs = getHyphenationChoreo.newInputSet();

			// Set inputs
			getHyphenationInputs.set_Word(parsedSentence[i]);
			getHyphenationInputs.set_APIKey(wordnik_api_key);
	
			// Execute Choreo
			GetHyphenationResultSet getHyphenationResults = getHyphenationChoreo.execute(getHyphenationInputs);
	
			// Get the response
			String hyphenationResult = getHyphenationResults.get_Response();
	
			// Interpret the response
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(hyphenationResult);
			JsonArray rootArray = root.getAsJsonArray();
	
			int syllableCount = 0;
			for ( JsonElement syllableElement : rootArray) {
				syllableCount++;
			}


			// Print the word and its syllable count if it has 3 or more syllables (polysyllabic)
			if (syllableCount >= 3) {
				polysyllabicWords[i] =  parsedSentence[i];
				nSyllables[i] = Integer.toString(syllableCount);
				System.out.println(parsedSentence[i] + "\t" + syllableCount);
			}
		}

		String[] results = polysyllabicWords;
		return results;
	}
}
