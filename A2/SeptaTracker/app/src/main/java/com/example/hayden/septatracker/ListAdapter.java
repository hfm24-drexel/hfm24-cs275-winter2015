package com.example.hayden.septatracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ListAdapter extends ArrayAdapter< StationSchedule > {
    /*
        Custom array adapter, which takes a list of stationSchedules as its data input.
        The adapter fills in a listView, which has as its items a list of the forecast's
        date, condition image (cloudy, sunny, rain, etc.), condition, temperature, and humidity.
     */


    private final Context context;
    private final List<StationSchedule> stationSchedules;

    public ListAdapter(Context context, List<StationSchedule> stationSchedules) {
        super(context, R.layout.activity_main, stationSchedules);
        this.context = context;
        this.stationSchedules = stationSchedules;
    }

    @Override
    public View getView(final int position,
                        View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // view which fills in the necessary forecast information in the list_item_layout
        View rowView = inflater.inflate(R.layout.list_item_layout,
                parent, false);

        // Getting all the views in rowView
        TextView train_no = (TextView) rowView.findViewById(R.id.textView);
        TextView departure_time = (TextView) rowView.findViewById(R.id.textView2);
        TextView arrival_time = (TextView) rowView.findViewById(R.id.textView3);
        TextView delay = (TextView) rowView.findViewById(R.id.textView4);

        // Setting the text views in rowView
        train_no.setText("Train No.: " + stationSchedules.get(position).getOrig_train());
        departure_time.setText("Scheduled Departure: " + stationSchedules.get(position).getOrig_departure_time());
        arrival_time.setText("Scheduled Arrival: " + stationSchedules.get(position).getOrig_arrival_time());
        delay.setText("Delay: " + stationSchedules.get(position).getDelay());

        return rowView;
    }

}
