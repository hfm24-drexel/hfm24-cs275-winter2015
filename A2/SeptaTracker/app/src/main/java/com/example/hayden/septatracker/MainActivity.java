package com.example.hayden.septatracker;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_MESSAGE = "com.mycompany.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        TextView text = (TextView) findViewById(R.id.textView3);
        final Spinner source_station_spinner = (Spinner) findViewById(R.id.spinner);
        final Spinner destination_station_spinner = (Spinner) findViewById(R.id.spinner2);
        Button searchButton = (Button) findViewById(R.id.button);

        List<String> stations = new ArrayList<String>();

        try {
            stations = getStationList();
        } catch(Exception e) {
            //text.setText(e.getMessage());
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        source_station_spinner.setAdapter(adapter);
        destination_station_spinner.setAdapter(adapter);

        final List<StationSchedule> stationSchedules = new ArrayList<StationSchedule>();
        final ListAdapter stationAdapter = new ListAdapter(this, stationSchedules);

        ListView l = (ListView) findViewById(R.id.listView);
        l.setAdapter(stationAdapter);

        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                stationSchedules.clear();
                NextToArrive nextToArrive = new NextToArrive(stationAdapter,
                        stationSchedules,
                        source_station_spinner.getSelectedItem().toString().replaceAll(" ", "%20"),
                        destination_station_spinner.getSelectedItem().toString().replaceAll(" ", "%20"));
                nextToArrive.execute();
            }
        });

        l.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                String train_no = arg1.findViewById(R.id.textView).toString();
                sendMessage(arg1, train_no);
                return true;
            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public List<String> getStationList() throws IOException {
        String filePath = "station_id_name.csv";
        String csvSplitBy = ",";
        List<String> stationNames = new ArrayList<String>();

        TextView text = (TextView) findViewById(R.id.textView3);

        byte[] buffer = null;

        InputStream is;
        try {
            is = getAssets().open(filePath);
            int size = is.available(); //size of the file in bytes
            buffer = new byte[size]; //declare the size of the byte array with size of the file
            is.read(buffer); //read file
            is.close(); //close file

        } catch (IOException e) {
            text.setText(e.getMessage());

        }

        String[] stationsText = new String(buffer).split("\n");
        for (int i = 0; i < stationsText.length; i++) {
            String[] id_and_name = stationsText[i].split(csvSplitBy);
            stationNames.add(id_and_name[1]);
        }

        return stationNames;
    }

    public void sendMessage(View view, String train_no) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        intent.putExtra(EXTRA_MESSAGE, train_no);
        startActivity(intent);
    }
}


