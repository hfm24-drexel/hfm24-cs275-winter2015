package com.example.hayden.septatracker;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


/**
 * Created by Hayden on 3/9/2015.
 */
public class AdditionalTrainInfo extends AsyncTask<Void, Void, Void> {

    private String train_no;
    TextView text_last_stop, text_sched_tm, text_act_tm;
    public AdditionalTrainInfo (String _train_no,
                                TextView last_stop,
                                TextView sched_tm,
                                TextView act_tm) {
        train_no = _train_no;
        text_last_stop = last_stop;
        text_sched_tm = sched_tm;
        text_act_tm = act_tm;

        text_last_stop.setText("Last Stop: Not Departed Yet");
        text_sched_tm.setText("Scheduled Departure Time: Not Departed Yet");
        text_act_tm.setText("Actual Departure Time: Not Departed Yet");
    }

    @Override
    protected Void doInBackground(Void... params) {
        // Networking...
        try {

            String sURL = "http://www3.septa.org/hackathon/NextToArrive/RRSchedules/" + train_no;
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            // Convert to a JSON object to print data
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonArray rootobj = root.getAsJsonArray();

            // Get individual train schedules and add them to the list
            String prev_act_tm = "";
            String sched_tm = "";
            String last_stop = "";
            String act_tm = "";
            for ( JsonElement station : rootobj) {

                JsonObject st = station.getAsJsonObject();
                act_tm = st.get("act_tm").getAsString();
                if (act_tm == "na") {

                    text_last_stop.setText("Last Stop: " + last_stop);
                    text_sched_tm.setText("Scheduled Departure Time: " + sched_tm);
                    text_act_tm.setText("Actual Departure Time: " + prev_act_tm);

                    break;
                }
                else {
                    prev_act_tm = st.get("act_tm").getAsString();
                    sched_tm = st.get("sched_tm").getAsString();
                    last_stop = st.get("station").getAsString();
                }
            }

            text_last_stop.setText("Last Stop: " + last_stop);
            text_sched_tm.setText("Scheduled Departure Time: " + sched_tm);
            text_act_tm.setText("Actual Departure Time: " + act_tm);

        } catch(Exception e) {
            Log.e("ERROR", e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void params) {

    }
}
