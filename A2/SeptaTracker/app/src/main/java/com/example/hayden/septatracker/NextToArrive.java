package com.example.hayden.septatracker;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


/**
 * Created by Hayden on 3/9/2015.
 */
public class NextToArrive extends AsyncTask<Void, Void, Void> {

    private ListAdapter adapter;
    private List<StationSchedule> schedules;
    private String source, destination;

    public NextToArrive(ListAdapter _adapter,
                        List<StationSchedule> _schedules,
                        String _source,
                        String _destination) {
        adapter = _adapter;
        schedules = _schedules;
        source = _source;
        destination = _destination;
    }

    @Override
    protected Void doInBackground(Void... params) {
        // Networking...
        try {

                int trainLimit = 24;
                String sURL = "http://www3.septa.org/hackathon/NextToArrive/" + source + "/" + destination + "/" + trainLimit;
                URL url = new URL(sURL);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.connect();

                // Convert to a JSON object to print data
                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                JsonArray rootobj = root.getAsJsonArray();

                // Get individual train schedules and add them to the list
                for ( JsonElement station : rootobj) {

                    JsonObject st = station.getAsJsonObject();
                    String orig_train= st.get("orig_train").getAsString();
                    String orig_departure_time = st.get("orig_departure_time").getAsString();
                    String orig_arrival_time = st.get("orig_arrival_time").getAsString();
                    String delay = st.get("orig_delay").getAsString();

                    StationSchedule newStationSchedule = new StationSchedule(orig_train, orig_departure_time, orig_arrival_time, delay);
                    schedules.add(newStationSchedule);
                }

        } catch(Exception e) {
            Log.e("ERROR", e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void params) {

        // Updating the adapter
        adapter.notifyDataSetChanged();
    }
}

