package com.example.hayden.septatracker;

/**
 * Created by Hayden on 3/11/2015.
 */
public class StationSchedule {
    private String orig_train, orig_departure_time, orig_arrival_time, delay;

    public StationSchedule(String orig_train,
                           String orig_departure_time,
                           String orig_arrival_time,
                           String delay) {
        this.orig_train = orig_train;
        this.orig_departure_time = orig_departure_time;
        this.orig_arrival_time = orig_arrival_time;
        this.delay = delay;
    }

    public StationSchedule() {}

    public String getOrig_train() {
        return orig_train;
    }

    public void setOrig_train(String orig_train) {
        this.orig_train = orig_train;
    }

    public String getOrig_departure_time() {
        return orig_departure_time;
    }

    public void setOrig_departure_time(String orig_departure_time) {
        this.orig_departure_time = orig_departure_time;
    }

    public String getOrig_arrival_time() {
        return orig_arrival_time;
    }

    public void setOrig_arrival_time(String orig_arrival_time) {
        this.orig_arrival_time = orig_arrival_time;
    }

    public String getDelay() {
        return delay;
    }

    public void setDelay(String delay) {
        this.delay = delay;
    }
}
