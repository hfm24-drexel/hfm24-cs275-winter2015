package com.example.hayden.septatracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AdditionalTrainInfoAdapter extends ArrayAdapter< ExtraTrainInfo > {
    /*
        Custom array adapter, which takes a list of stationSchedules as its data input.
        The adapter fills in a listView, which has as its items a list of the forecast's
        date, condition image (cloudy, sunny, rain, etc.), condition, temperature, and humidity.
     */


    private final Context context;
    private final List<ExtraTrainInfo> extraTrainInfos;


    public AdditionalTrainInfoAdapter(Context context,
                                      List<ExtraTrainInfo> trainInfo) {
        super(context, R.layout.activity_display_message, trainInfo);
        this.context = context;
        extraTrainInfos = trainInfo;

    }

    @Override
    public View getView(final int position,
                        View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // view which fills in the necessary forecast information in the list_item_layout
        View rowView = inflater.inflate(R.layout.list_item_layout,
                parent, false);

        // Getting all the views in rowView
        TextView last_stop = (TextView) rowView.findViewById(R.id.textView);
        TextView sched_tm = (TextView) rowView.findViewById(R.id.textView2);
        TextView act_tm = (TextView) rowView.findViewById(R.id.textView3);

        // Setting the text views in rowView
        last_stop.setText("Train No.: " + extraTrainInfos.get(position).getLast_stop());
        sched_tm.setText("Scheduled Departure: " + extraTrainInfos.get(position).getSched_tm());
        act_tm.setText("Scheduled Arrival: " + extraTrainInfos.get(position).getAct_tm());

        return rowView;
    }

}
