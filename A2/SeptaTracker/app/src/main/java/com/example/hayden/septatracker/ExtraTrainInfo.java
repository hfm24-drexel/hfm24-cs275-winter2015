package com.example.hayden.septatracker;

/**
 * Created by Hayden on 3/12/2015.
 */
public class ExtraTrainInfo {
    private String last_stop, sched_tm, act_tm;

    public ExtraTrainInfo(String last_stop,
                          String sched_tm,
                          String act_tm) {

        this.last_stop = last_stop;
        this.sched_tm = sched_tm;
        this.act_tm = act_tm;
    }

    public ExtraTrainInfo() {

    }

    public String getLast_stop() {
        return last_stop;
    }

    public void setLast_stop(String last_stop) {
        this.last_stop = last_stop;
    }

    public String getSched_tm() {
        return sched_tm;
    }

    public void setSched_tm(String sched_tm) {
        this.sched_tm = sched_tm;
    }

    public String getAct_tm() {
        return act_tm;
    }

    public void setAct_tm(String act_tm) {
        this.act_tm = act_tm;
    }
}
