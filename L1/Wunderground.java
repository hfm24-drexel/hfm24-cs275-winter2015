import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

// Requires gson jars
import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Wunderground {         
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// Get from http://www.wunderground.com/weather/api/
		String key;
		if(args.length < 1) {
			System.out.println("Enter key: ");
			
			Scanner in = new Scanner(System.in);
			key = in.nextLine();
		} else {
			key = args[0];
		}
		
		String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";
		
		// Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		// Convert to a JSON object to print data
    		JsonParser jp = new JsonParser();
    		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    		JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
    	
    		// Get zip code from the input key
    		int zip = rootobj.get("location").getAsJsonObject().get("zip").getAsInt();
		System.out.println("Zip Code: " + zip);
	
		// Connect to new url using the key and zip to get the hourly forecast
		sURL = "http://api.wunderground.com/api/" + key + "/hourly/q/" + zip + ".json";	
		url = new URL(sURL);
		request = (HttpURLConnection) url.openConnection();
		request.connect();
	
		// Convert to a Json object to print data
		jp = new JsonParser();
		root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		rootobj = root.getAsJsonObject();
	
		// Get the hourly forecast
		JsonArray hourly_forecast = rootobj.get("hourly_forecast").getAsJsonArray();
		// Loop through the hourly forecast array, getting the date, condition, temperature, and humidity
		for (JsonElement forecast : hourly_forecast)
		{
			JsonObject fc = forecast.getAsJsonObject();
			String date = fc.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
			String condition = fc.getAsJsonObject().get("condition").getAsString();
			int temp_f = fc.getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsInt();
			int humidity = fc.getAsJsonObject().get("humidity").getAsInt();
	
			System.out.print(date + "\n");
			System.out.print("Condition: " + condition + "\n");
			System.out.print("Temperature: " + temp_f + " Farenheit\n");
			System.out.print("Humidity: " + humidity + "\n\n");
		}

	}

}
