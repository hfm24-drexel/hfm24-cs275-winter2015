import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import com.temboo.Library.Google.OAuth.InitializeOAuth;
import com.temboo.Library.Google.OAuth.FinalizeOAuth;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;





public class googleCalendarLister {
	
	static String clientID, clientSecret, redirect_uri, acctName, appKeyName, appKeyValue;

	
	public static String executePost( String targetURL, String params) {
	
		URL url;
		HttpURLConnection connection = null;

		try {
			url = new URL(targetURL);

			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(params.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
			wr.writeBytes(params);
			wr.flush();
			wr.close();

			// Get response
			InputStream is = connection.getInputStream();	
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
		finally {
			
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	
	public static void main ( String [] args ) throws TembooException {
		

		// Getting user input for manual and temboo authorization
		Scanner in = new Scanner(System.in);

		System.out.println("Enter Client ID: ");
		clientID = in.nextLine();

		System.out.println("Enter Client Secret: ");
		clientSecret = in.nextLine();

		System.out.println("Enter Redirect URI: ");
		redirect_uri = in.nextLine();

		System.out.println("Enter Temboo Account Name: ");
		acctName = in.nextLine();
		System.out.println("Enter Temboo App Key Name: ");
		appKeyName = in.nextLine();
		System.out.println("Enter Temboo App Key Value");
		appKeyValue = in.nextLine();

		// Done getting manual input
		try {
			part1(); // Manual
			part2(); // Temboo
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void part1 () throws Exception {
		
		Scanner in = new Scanner(System.in);

		String sURL = "http://accounts.google.com/o/oauth2/auth?" +
				"access_type=offline" +
				"&client_id=" + clientID + 
				"&response_type=code" +	
				"&scope=https://www.googleapis.com/auth/calendar" +
				"&redirect_uri=" + redirect_uri + 
				"&state=/profile" + 
				"&approvalprompt=force";
		
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();

		System.out.println("Go here: " + sURL);
		System.out.println("Enter the code you got: ");
		String code = in.nextLine();

		String authorizeURL = "https://accounts.google.com/o/oauth2/token";
		String authorizeParams = "code=" + code + "&client_id=" + clientID + "&client_secret=" + clientSecret + "&redirect_uri=" + redirect_uri + "&grant_type=authorization_code";
		String authorizeResponse = executePost(authorizeURL, authorizeParams);
		
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(authorizeResponse);
		JsonObject rootobj = root.getAsJsonObject();
		String access_token = rootobj.get("access_token").getAsString();
		System.out.println("\nOAuth access token: " + access_token);

		String sCalendar_url = "https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token=" + access_token;
		url = new URL(sCalendar_url);
		request = (HttpURLConnection) url.openConnection();
		request.connect();
		InputStream calendarList = (InputStream) (request.getContent());

		jp = new JsonParser();
		root = jp.parse(new InputStreamReader(calendarList));
		rootobj = root.getAsJsonObject();
		JsonArray items = rootobj.get("items").getAsJsonArray();

		int i = 0;
		for (JsonElement calendar : items) {
			i++;
			JsonObject cldr = calendar.getAsJsonObject();
			

			String summary = cldr.get("summary").getAsString();
			System.out.println("\nCalendar " + i + " summary: " + summary);
			if (i == 1) {
				JsonArray events = cldr.get("notificationSettings").getAsJsonObject().get("notifications").getAsJsonArray();

				int j = 0;
				for (JsonElement notification : events) {
					j++;
					String type = notification.getAsJsonObject().get("type").getAsString();
					String method = notification.getAsJsonObject().get("method").getAsString();
					System.out.println("\tNotification " + j + ": ");
					System.out.println("\t\tType: " + type + "\n\t\tMethod: " + method);
				}
			}
		}
	}

	public static void part2 () throws TembooException {

		Scanner in = new Scanner(System.in);

		TembooSession session = new TembooSession(acctName, appKeyName, appKeyValue);
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
		
		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
		
		// Set inputs
		initializeOAuthInputs.set_ClientID(clientID);
		initializeOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar");
		
		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);	

		
		//
		String callbackID = initializeOAuthResults.get_CallbackID();
		String authorizationURL = initializeOAuthResults.get_AuthorizationURL();
		
		System.out.println("Go here: " + authorizationURL);
		System.out.println("Type 'done' to confirm that you have authorized the application: ");
		String response = in.nextLine();
		
		//


		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callbackID);
		finalizeOAuthInputs.set_ClientSecret(clientSecret);
		finalizeOAuthInputs.set_ClientID(clientID);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);

		String access_token = finalizeOAuthResults.get_AccessToken();

	 	GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

                // Get an InputSet object for the choreo                
		GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();
                // Set inputs
                getAllCalendarsInputs.set_ClientSecret(clientSecret);
                getAllCalendarsInputs.set_AccessToken(access_token);                
		getAllCalendarsInputs.set_ClientID(clientID);

                // Execute Choreo                
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);

                // Now parse the json
        	JsonParser jp = new JsonParser();
        	JsonElement root = jp.parse(getAllCalendarsResults.get_Response());        
		JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

        	JsonArray items = rootobj.get("items").getAsJsonArray();
        	for(int i = 0; i < items.size(); i++) {
        	        JsonObject item = items.get(i).getAsJsonObject();                
			String id = item.get("id").getAsString();
        	        System.out.println("Found calendar id: " + id);
			if (i == 0) {

				JsonArray events = item.get("notificationSettings").getAsJsonObject().get("notifications").getAsJsonArray();
				int j = 0;
                                for (JsonElement notification : events) {
                                        j++;
                                        String type = notification.getAsJsonObject().get("type").getAsString();
                                        String method = notification.getAsJsonObject().get("method").getAsString();
                                        System.out.println("\tNotification " + j + ": ");
                                        System.out.println("\t\tType: " + type + "\n\t\tMethod: " + method);
                                }

			}
        	}
	}
}
